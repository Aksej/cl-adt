(in-package #:cl-adt)

(defstruct adt
  ctors creator)

(defstruct adt-ctor
  name adt slots)

(let ((adts (make-hash-table :test #'eq))
      (ctors (make-hash-table :test #'eq)))
  (defun find-adt (name)
    (declare (type symbol name))
    (the (values (or adt null)
		 boolean)
      (gethash name adts)))

  (defun find-ctor (name)
    (declare (type symbol name))
    (the (values (or adt-ctor null)
		 boolean)
      (gethash name ctors)))

  (defun (setf find-adt)
      (val name)
    (declare (type (or adt null)
		   val)
	     (type symbol name))
    (the (or adt null)
      (if val
	  (setf (gethash name adts)
		val)
	  (remhash name adts))))

  (defun (setf find-ctor)
      (val name)
    (declare (type (or adt-ctor null)
		   val)
	     (type symbol name))
    (the (or adt-ctor null)
      (if val
	  (setf (gethash name ctors)
		val)
	  (remhash name ctors))))
  )

(defun adt-exists? (name)
  (nth-value 1 (find-adt name)))

(defun conflicting? (adt-name &rest ctor-names)
  (loop for ctor in ctor-names
     do (aif (find-ctor ctor)
	     (unless (eq (adt-ctor-adt it)
			 adt-name)
	       (return it)))))

(defun valid-name? (symbol)
  (and (symbolp symbol)
       (not (or (eq symbol nil)
		(eq symbol t)
		(eq symbol 'quote)
		(keywordp symbol)))))

(defun register-adt (adt-name &rest ctor-specs)
  #+nil(when (redefining? adt-name)
	 (warn "Redefining ADT ~S"
	       adt-name))
  (let ((ctor-names (mapcar #'car-or-x
			    ctor-specs)))
    (aif (apply #'conflicting?
		adt-name ctor-names)
	 (error "Constructor name conflict while defining ADT ~S for
constructor ~S which is already defined for ADT ~S."
		adt-name (adt-ctor-name it)
		(adt-ctor-adt it)))
    (setf (find-adt adt-name)
	  (make-adt :ctors ctor-names
		    :creator (mk-adt-symbol adt-name)))
    (loop for ctor-spec in ctor-specs
       do (setf (find-ctor (car-or-x ctor-spec))
		(make-adt-ctor :adt adt-name
			       :slots (mapcar (lambda (sym-or-cons)
						(mk-adt-symbol sym-or-cons))
					      (cdr-or-nil
					       ctor-spec))))))
  adt-name)
