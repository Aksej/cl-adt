;;;; package.lisp

(defpackage #:cl-adt
  (:use #:cl #:cl-lazy-object)
  (:shadowing-import-from
   #:closer-mop
   #:slot-definition-name #:class-slots #:find-class
   )
  (:shadowing-import-from
   #:fset
   #:compare #:equal?)
  (:export
   #:match #:defmatch #:defadt #:$
   ))

(defpackage #:cl-adt-symbols)
