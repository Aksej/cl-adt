(in-package #:cl-adt)

(defun adt-type? (val)
  (and (symbolp val)
       (or (find-adt val)
	   (find-ctor val))))

(defun adt-ctor? (val)
  (and (symbolp val)
       (find-ctor val)))

(defun wild? (val)
  (and (symbolp val)
       (let ((n (symbol-name val)))
	 (and (> (length n)
		 0)
	      (char= (elt n 0)
		     #\_)))))

(defun complex-ctor? (val)
  (and (consp val)
       (let ((desc (first val)))
	 (and (symbolp desc)
	      (let ((ctor (find-ctor desc)))
		(and ctor (adt-ctor-slots ctor)))))))

(defun arg-list? (val)
  (and (consp val)
       (not (complex-ctor? val))
       (not (eq (first val)
		'quote))))

(defun variable? (val)
  (and (symbolp val)
       (not (or (adt-type? val)
		(wild? val)))))

(defun constant? (val)
  (and (or (not (symbolp val))
	   (keywordp val))
       (or (not (consp val))
	   (eq (first val)
	       'quote))))
