(in-package #:cl-adt)

(defparameter cl-adt-symbols::*count* 0)

(defun mk-adt-symbol (&optional (name "adt-symbol"))
  (incf cl-adt-symbols::*count*)
  (intern (format nil "~A~D"
                  name cl-adt-symbols::*count*)
          '#:cl-adt-symbols))

(defun car-or-x (x)
  (if (consp x)
      (car x)
      x))

(defun cdr-or-nil (x)
  (when (consp x)
    (cdr x)))

(defun ensure-list (x)
  (if (listp x)
      x
      (list x)))

(defun length= (seq1 seq2)
  (if (and (listp seq1)
	   (listp seq2))
      (if (not seq1)
	  (unless seq2
	    t)
	  (when seq2
	    (length= (rest seq1)
		     (rest seq2))))
      (= (length seq1)
	 (length seq2))))

(defun flatten (tree)
  (when tree
    (if (consp tree)
	(loop for subtree in tree
	   append (flatten subtree))
	(list tree))))

(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it
	 ,then
	 ,else)))

(defmacro vaif (form then &optional else)
  (let ((g!found? (gensym "found?")))
    `(multiple-value-bind (val ,g!found?)
	 ,form
       (if ,g!found?
	   ,then
	   ,else))))
