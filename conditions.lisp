(in-package #:cl-adt)

(defun ctor-cond (ctor slot-matches argstruct)
  (let ((slots (adt-ctor-slots ctor)))
    (loop for slot in slots
       for slot-match in slot-matches
       append (make-cond slot-match `(,slot (force ,argstruct))))))

(defun make-cond (matcher argstruct)
  (cond
    ((constant? matcher)
     `((equal? ,matcher ,argstruct)))
    ((adt-ctor? matcher)
     `((typep (force ,argstruct)
	      ',matcher)))
    ((adt-type? matcher)
     `((typep ,argstruct ',matcher)))
    ((complex-ctor? matcher)
     `((typep (force ,argstruct)
	      ',(first matcher))
       ,@(ctor-cond (find-ctor (first matcher))
		    (rest matcher)
		    argstruct)))
    ((arg-list? matcher)
     (unless (length= matcher argstruct)
       (error "Arglist length mismatch ~S ~S"
	      matcher argstruct))
     (loop for submatch in matcher
	for subarg in argstruct
	append (make-cond submatch subarg)))))