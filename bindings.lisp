(in-package #:cl-adt)

(defun ctor-bind (ctor slot-matches argstruct)
  (let ((slots (adt-ctor-slots ctor)))
    (loop for slot in slots
       for slot-match in slot-matches
       append (make-bind slot-match `(,slot (force ,argstruct))))))

(defun make-bind (matcher argstruct)
  (cond
    ((variable? matcher)
     `((,matcher (force ,argstruct))))
    ((complex-ctor? matcher)
     (ctor-bind (find-ctor (first matcher))
		(rest matcher)
		argstruct))
    ((arg-list? matcher)
     (unless (length= matcher argstruct)
       (error "Arglist length mismatch ~S ~S"
	      matcher argstruct))
     (loop for submatch in matcher
	for subarg in argstruct
	append (make-bind submatch subarg)))
    (t '())))
