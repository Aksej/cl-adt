(in-package #:cl-adt)

(defmacro match-helper (params
			&body cases)
  (if cases
      (let* ((curr (first cases))
	     (rest (rest cases))
	     (matcher (first curr))
	     (body (rest curr)))
	`(if (and ,@(make-cond matcher params))
	     (let ,(make-bind matcher params)
	       (values ,@body))
	     (match-helper ,params ,@rest)))
      `(error "Couldn't match ~A"
	      ',params)))

(defmacro match (params result-type
		 &body cases)
  (let ((result-type-complete (if (and (consp result-type)
                                       (eq (car result-type)
                                           'values))
                                  result-type
                                  `(values ,result-type &rest nil))))
    `(the ,result-type-complete
       ,(aif (and (symbolp result-type)
                  (find-adt result-type))
             `(,(adt-creator it)
                (match-helper ,params ,@cases))
             `(match-helper (,@params)
                ,@cases)))))

(defun ensure-arglist (matcher)
  (if (not (arg-list? matcher))
      (list matcher)
      matcher))

(defmacro defmatch (name (&rest argtypes)
		    result-type
		    &body cases)
  (let ((args (mapcar (lambda (sym)
			(mk-adt-symbol sym))
                      argtypes)))
    `(defmethod ,name ,(mapcar #'list
			       args argtypes)
       (match (,@args)
	   ,result-type
	 ,@(mapcar (lambda (case)
		     (cons (ensure-arglist (car case))
			   (cdr case)))
		   cases)))))
