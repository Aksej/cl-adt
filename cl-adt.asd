;;;; cl-adt.asd

(asdf:defsystem #:cl-adt
  :serial t
  :description "Algebraic data types for Common Lisp"
  :author "Thomas Bartscher <thomas.bartscher@weltraumschlangen.de>"
  :license "EUPL V1.1"
  :depends-on (#:cl-lazy-object
               #:closer-mop
	       #:fset
	       )
  :components
  ((:file "package")
   (:file "utilities")
   (:file "registry")
   (:file "match-types")
   (:file "bindings")
   (:file "conditions")
   (:file "define-algebraic")
   (:file "match")
   ))
