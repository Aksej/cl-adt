(in-package #:cl-adt)

(defun find-slots (ctor-name)
  (aif (find-ctor ctor-name)
       (values (adt-ctor-slots it)
	       t)
       (values nil nil)))

(defgeneric force (obj)
  (:method (obj)
    obj))

(defgeneric comp (obj))
(defgeneric val (obj))

(defun adt-class-form (adt-name)
  `(defclass ,adt-name ()
     ((%comp :type (or function null)
	     :initarg :comp
	     :accessor comp)
      (%val :accessor val))))

(defun adt-creator-form (adt-name)
  `(defmacro ,(adt-creator (find-adt adt-name))
       (comp)
     `(make-instance ',',adt-name
		     :comp (lambda ()
			     (force ,comp)))))

(defun adt-force-form (adt-name ctors)
  `(defmethod force ((obj ,adt-name))
     (aif (comp obj)
	  (let ((result (funcall it)))
	    (assert (typep result '(or ,@ctors)))
	    (setf (comp obj)
		  nil
		  (val obj)
		  result))
	  (val obj))))

(defun adt-print-form (adt-name)
  `(defmethod print-object ((object ,adt-name)
			    stream)
     (format stream "~A"
	     (print-object (force object)
			   nil))))

(defun adt-compare-form (adt-name)
  `(defmethod compare ((x ,adt-name)
		       (y ,adt-name))
     (let ((x (force x))
	   (y (force y)))
       (if (and (eq (class-of x)
                    (class-of y))
                (equal? x y))
	   :equal
	   (call-next-method x y)))))

(defun ctor-class-form (adt-name name slots)
  `(deflazytype ,name (,adt-name)
     ,@(mapcar (lambda (slot-name slot-type)
		 `(,slot-name :type ,slot-type))
	       (adt-ctor-slots (find-ctor name))
	       slots)))

(defun ctor-force-form (name)
  `(defmethod force ((obj ,name))
     obj))

(defun ctor-print-form (ctor-name)
  `(defmethod print-object ((object ,ctor-name)
			    stream)
     ,(aif (find-slots ctor-name)
	   `(format stream ,(format nil "~A~S~A"
				    "~:@<(" ctor-name "~;~{ ~S~}~:@>")
		    (list ,@(mapcar (lambda (slot)
				      `(,slot object))
				    it)))
	   `(format stream ,(format nil "~S"
				    ctor-name)))))

(defun ctor-compare-form (adt-name ctor-name1 ctor-name2)
  `(defmethod compare ((x ,ctor-name1)
                       (y ,ctor-name2))
     ,(if (eq ctor-name1 ctor-name2)
          (labels ((build-comparator (slots)
                     (let ((curr (first slots)))
                       (if (cdr slots)
                           `(let ((comparison (compare (,curr x)
                                                       (,curr y))))
                              (if (eq comparison :equal)
                                  ,(build-comparator (rest slots))
                                  comparison))
                           (if curr
                               `(compare (,curr x)
                                         (,curr y))
                               :equal)))))
            (build-comparator (adt-ctor-slots (find-ctor ctor-name1))))
          (if (member ctor-name2
                      (member ctor-name1 (adt-ctors (find-adt adt-name))))
              :greater
              :less))))

(defmacro define-algebraic (name &body constructor-specs)
  (eval-when (:compile-toplevel :load-toplevel :execute)
    (apply #'register-adt
           name constructor-specs))
  `(progn
     #+nil
     (eval-when (:execute)
       (apply #'register-adt
              ',name ',constructor-specs))
     ,(adt-class-form name)
     ,(adt-creator-form name)
     ,(adt-print-form name)
     ,(adt-compare-form name)
     ,@(mapcar (lambda (ctor-spec)
		 (let ((ctor-name (car-or-x ctor-spec)))
		   (remove nil
			   `(progn
			      ,(ctor-class-form name ctor-name
						(cdr-or-nil
						 ctor-spec))
			      ,(when (atom ctor-spec)
				     `(defparameter ,ctor-name
					(,ctor-name)))
			      ,(ctor-force-form ctor-name)
			      ,(ctor-print-form ctor-name)))))
	       constructor-specs)
     ,@(let ((constructor-names (mapcar #'car-or-x
                                        constructor-specs)))
         (loop :for ctor1 :in constructor-names
            :append
            (loop :for ctor2 :in constructor-names
               :collect
               (ctor-compare-form name ctor1 ctor2))))
     ,(adt-force-form name (mapcar #'car-or-x
				   constructor-specs))
     ',name))

(defmacro defadt (name &body constructor-specs)
  `(define-algebraic ,name
     ,@constructor-specs))

(defmacro $ ((ctor &rest args))
  `(let (,@(mapcar #'list
                   args args))
     (,ctor ,@args)))
